# esgBoost



## Description

Statistical package for variable selection and estimation of paramater using the gradient boosting algorithm. 

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Development](#development)
- [Commands](#commands)
- [Deployment](#deployment)
- [References](#references)

## Prerequisites

- [lmls](https://cran.r-project.org/web//packages//lmls/vignettes/lmls.pdf)
- [MASS](https://cran.r-project.org/web/packages/MASS/MASS.pdf)
- [gamlss](https://cran.r-project.org/web/packages/gamlss/gamlss.pdf)
- [gamboostLSS](https://cran.r-project.org/web/packages/gamboostLSS/vignettes/gamboostLSS_Tutorial.pdf)
<!--  - `>= 2.7`
 - [dplyr](https://rubygems.org/gems/bridgetown)  
  - `gem install bridgetown -N` 
- [Node](https://nodejs.org) 
  - `>= 12`
- [ggplot2](https://yarnpkg.com) -->

## Installation

<!--
```sh
 cd bridgetown-site-folder
 bundle install && yarn install
```
 Learn more: [Bridgetown Getting Started Documentation](https://www.bridgetownrb.com/docs/). -->

## Development

<!-- To start your site in development mode, run `bin/bridgetown start` and navigate to [localhost:4000](https://localhost:4000/)!

 Use a [theme](https://github.com/topics/bridgetown-theme) or add some [plugins](https://www.bridgetownrb.com/plugins/) to get started quickly.  -->

### Commands

<!--
```sh
# running locally
 bin/bridgetown start

# build & deploy to production
 bin/bridgetown deploy

# load the site up within a Ruby console (IRB)
 bin/bridgetown console
```
 > Learn more: [Bridgetown CLI Documentation](https://www.bridgetownrb.com/docs/command-line-usage) -->

### Deployment

<!-- You can deploy Bridgetown sites on hosts like Render or Vercel as well as traditional web servers by simply building and copying the output folder to your HTML root.

 > Read the [Bridgetown Deployment Documentation](https://www.bridgetownrb.com/docs/deployment) for more information. -->

## References

<!-- If repo is on GitHub:

 1. Fork it
 2. Clone the fork using `git clone` to your local development machine.
 3. Create your feature branch (`git checkout -b my-new-feature`)
 4. Commit your changes (`git commit -am 'Add some feature'`)
 5. Push to the branch (`git push origin my-new-feature`)
 6. Create a new Pull Request -->
