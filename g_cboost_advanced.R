######################################### Regular /quasi same results as above
library(lmls)
y <- mtcars$mpg
X <- data.matrix(data.frame(1, mtcars$cyl, mtcars$disp, mtcars$hp))
nu <- 0.1
mstop <- 1000000

boost <- function(y, X, nu, mstop){
  beta <- rep(0, ncol(X))
  for(m in 1:mstop){
    u <- y - X%*%beta
    b <- solve(crossprod(X))%*%t(X)%*%u
    beta <- beta + nu*b
  }
  return(beta)
}
boost(y, X, nu, mstop)
coef(summary(lm(y~X)))[,1:2]

######################################### Component-wise
comp_boost <- function(y, X, nu, mstop){ 
  #beta <- rep(0, ncol(X))               
  #gamma <- rep(0, ncol(X)) 
  
  beta <- c(mean(y), rep(0, ncol(X)-1))                # Step 1: (0)^beta_0 initialization for beta  (j = 0)
  gamma <- c(log(sd(y)), rep(0, ncol(X)-1))            #                    initialization for sigma (gamma)
  
  eta1 <- X%*%beta
  eta2 <- X%*%gamma
  
  for(m in 1:mstop){                                   # t = 0
    u1 <- (y - eta1) /  exp(2 * eta2)              # Step 2: the residuals are the negative gradients
    
    # Andres version: u2 <- t(X) %*% ((u1^2 / exp(2 * X %*% gamma)) - 1)                          
    # But dimensions works only if u2 <- ((u1^2 / exp(2 * X %*% gamma)) - 1)  
    u2 <- (- 1 + exp(-2*eta2) * ((y - eta1)^2))  
    # My calculation yields: 
    #u2 <- -(1/2) + (u1^2 / gamma^2)
    
    b1 <- c()                                          # Step 3.0: initilize b for the mean regression
    b2 <- c()                                          #    the same for the deviation regression
    RSS1 <- c()                                        # the RSSs are different ...
    RSS2 <- c()                                        # for different regressions
    # j = r, j = 0,...,k=ncol(X)
    for(r in 1:ncol(X)){                               
      Xr <- X[,r]
      b1[r] <- solve(crossprod(Xr))%*%t(Xr)%*%u1 # Step 3.1: ^b_j (mu) - Separate linear models ...
      RSS1[r] <- sum((u1 - Xr*b1[r])^2)             #                       for all covariates
      b2[r] <- solve(crossprod(Xr))%*%t(Xr)%*%u2 #           ^sigma_j
      RSS2[r] <- sum((u2 - X[,r]*b2[r])^2)
    }
    best1 <- which.min(RSS1)                           # Step 3.2: j^* (mu) - Best-fitting variable
    beta[best1] <- beta[best1] + nu*b1[best1]
    eta1 <- X%*%beta
    
    best2 <- which.min(RSS2)                           #           j^* (sigma)
    gamma[best2] <- gamma[best2] + nu*b2[best2]        
    eta2 <- X%*%gamma 
  }
  mat <- cbind(beta, gamma)
  #print(cbind(eta1, eta2))
  return(mat)
}
comp_boost(y, X, nu, mstop)
lmls_res <- coef(lmls(mpg ~ cyl+disp+hp, ~cyl+disp+hp, data = mtcars))
cbind(lmls_res$location, lmls_res$scale)

# Example 2
lmls_res <- coef(lmls(y~x, ~x, data = abdom))
cbind(lmls_res$location, lmls_res$scale)
comp_boost(abdom$y, cbind(1, abdom$x), nu, mstop)